Complete master mind project with test cases...

1. open master_mind python file, create an instance of the class MasterMind
2. Run play() function of MasterMind instance and enjoy the game..


The game is completely developed without UI... but can run on command line.

-> If you want to test the test cases, clone it to a folder and run 'test_master_mind' function.

