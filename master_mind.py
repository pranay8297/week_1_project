simport random

class MasterMind:
    def __init__(self):
        self.number = None
        self.guess = None
        self.length = None
    
    def number_split(self , number):
        arr = []
        for i in str(number):
            arr.append(int(i))
        return arr
    
    def c_cows(self , number , guess):
        cows = 0
        number_d = self.number_split(number)
        guess_d = self.number_split(guess)
        for i in guess_d:
            if i in number_d:
                cows += 1
                number_d.remove(i)
        return cows
    
    def c_bulls(self , number , guess):
        bulls = 0
        number_d = self.number_split(number)
        guess_d = self.number_split(guess)
        for i in range(self.length):
            if number_d[i] == guess_d[i]:
                bulls += 1
        return bulls

    def c_difference(self , number , guess):
        number_d = self.number_split(number)
        guess_d = self.number_split(guess)
        if number_d[0] == guess_d[0]:
            return 'low'
        elif abs(number_d[0] - guess_d[0]) <= 2:
            return 'high'
        else:
            return 'too high'
    
    def evaluate_guess(self , number , guess):
        bulls = self.c_bulls(self.number , self.guess)
        cows = self.c_cows(self.number , self.guess)
        distance = self.c_difference(self.number , self.guess)
        result = {'is_correct' : number == guess,
                  'distance' : distance,
                  'bulls' : bulls,
                  'cows' : cows - bulls}
        return result
    
    def reset(self , choice):
        if choice is 'y':
        	self.set_number()
            self.play()
            return True
        else:
            print('thank you...')
            return False
    
    def set_number(self , length):
        try:
            self.length = int(length)
        except ValueError:
            return {'number_accepted' : False
                    ,'ValueError' : True}
        if length == '0':
            return {'number_accepted': False,
                    'length' : 0}
        self.number = random.randrange(10 ** (int(length) - 1) , 10 ** int(length))
        return {'number_accepted': True,
                'self.number' : length}
    
    def input_guess(self , guess):
        try:
            int(guess)
        except ValueError:
            return {'number_accepted': False,
                    'ValueError': True}
        if len(guess) != self.length:
            return {'number_accepted': False,
                    'length': 'not matching'}
        else:
            self.guess = guess
            return{'number_accepted' : True,
                   'number': guess}
        
    def play(self, guess_value):
        input_check = self.input_guess(guess_value)
        if input_check['number_accepted'] is True:
            self.guess = guess_value
        else:
            return input_check
        result = self.evaluate_guess(self.number , self.guess)
        return result
    
        
        

    
    
        
            
        
    



    

    
    
    
    
            
            
