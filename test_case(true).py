from master_mind import MasterMind

def test_master_mind():
    
    game = MasterMind()
    #test case 1--- testing number_set function 
    test_length = '2'
    test_result = game.set_number(test_length)
    result = {'number_accepted': True,
                'self.number' : '2'}
    
    assert test_result == result
    
    #test case 2-- test number_set function
    
    test_length = 'abc'
    result = {'number_accepted' : False
                    ,'ValueError' : True}
    test_result = game.set_number(test_length)
    
    assert result == test_result
    
    #test case 2.1-- test number_set function
    
    test_length = '0'
    result = {'number_accepted': False,
                    'length' : 0}
    test_result = game.set_number(test_length)
    
    assert result == test_result
    
    #test case --3 test input_guess function(checking weather the input is number or not)
    game.length = 5
    test_guess = '12345'
    result = {'number_accepted' : True,
                   'number': test_guess}
    test_result = game.input_guess(test_guess)
    assert result == test_result
    
    #test case --3 test input_guess function(checking weather the input is number or not)
    game.length = 5
    test_guess = 'tdg45'
    result = {'number_accepted': False,
                    'ValueError': True}
    test_result = game.input_guess(test_guess)
    assert result == test_result
    
    #test case - 4 test the length of the inputted string
    game.length = 5
    test_guess = '123' #length = 3
    result = {'number_accepted': False,
                    'length': 'not matching'}
    test_result = game.input_guess(test_guess)
    assert result == test_result
    
    #test case -5 test the working of guess function
    game.length = 5
    game.number = '34567'
    game.guess = '34567'
    result = {'is_correct':True,
              'distance': 'low',
              'bulls' : 5 ,
              'cows' : 0}
    test_result = game.evaluate_guess(game.number , game.guess)
    assert result == test_result
    
    #test case -6 test the working of the guess function at the extremes
    game.length = 5
    game.number = '34567'
    game.guess = '00000'
    result = {'is_correct':False,
              'distance': 'too high',
              'bulls' : 0 ,
              'cows' : 0}
    test_result = game.evaluate_guess(game.number , game.guess)
    assert test_result == result
    
    
    #test case -7.2 test the working of the guess function at the extremes
    game.length = 5
    game.number = '34567'
    game.guess = '99999'
    result = {'is_correct':False,
              'distance': 'too high',
              'bulls' : 0 ,
              'cows' : 0}
    test_result = game.evaluate_guess(game.number , game.guess)
    assert result == test_result
    
    #test case -7.2 test the working of the guess function at the extremes
    game.length = 5
    game.number = '99999'
    game.guess = '99999'
    result = {'is_correct':True,
              'distance': 'low',
              'bulls' : 5 ,
              'cows' : 0}
    test_result = game.evaluate_guess(game.number , game.guess)
    assert result == test_result
    
    
    #test case - 8 test the working of guess function at intermediate levels
    game.length = 5
    game.number = '34567'
    game.guess = '56789'
    result = {'is_correct': False,
              'distance': 'high',
              'bulls' : 0 ,
              'cows' : 3}
    test_result = game.evaluate_guess(game.number , game.guess)
    assert result == test_result
    
    #testing reset function
    choice = 'y'
    result = True
    test_result = game.reset(choice)
    assert result == test_result
    
    print('\n all test cases successful')

test_master_mind()

